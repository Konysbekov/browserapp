package com.example.browserapp

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.example.browserapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                binding.toolbar.title = view?.title ?: "My Browser"
            }
        }
        binding.webView.settings.javaScriptEnabled = true

        binding.searchButton.setOnClickListener {
            val url = binding.urlEditText.text.toString()
            if (url.isNotEmpty()) {
                binding.webView.loadUrl(url)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        if (resources.configuration.orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE) {
            inflater.inflate(R.menu.main_menu_land, menu)
        } else {
            inflater.inflate(R.menu.main_menu, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val color = when (item.itemId) {
            R.id.menu_blue -> "#0000FF"
            R.id.menu_green -> "#00FF00"
            R.id.menu_violet -> "#EE82EE"
            R.id.menu_brown -> "#A52A2A"
            R.id.menu_yellow -> "#FFFF00"
            R.id.menu_orange -> "#FFA500"
            else -> return super.onOptionsItemSelected(item)
        }
        binding.searchButton.setBackgroundColor(Color.parseColor(color))
        return true
    }
}
